def f(totalWeight, weights, values, number): 
    if number == 0 or totalWeight == 0 :
        return 0
  
    if (weights[number-1] > totalWeight): 
        return f(totalWeight, weights, values, number-1) 
  
    else:
        return max(values[number-1] + f(totalWeight-weights[number-1], weights, values, number-1), f(totalWeight, weights, values, number-1) ) 

totalWeight,number = [int(x) for x in input().split()]
values = [int(x) for x in input().split()] 
weights = [int(x) for x in input().split()] 

print(f(totalWeight, weights, values, number))