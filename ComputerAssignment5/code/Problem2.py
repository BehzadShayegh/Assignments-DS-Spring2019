from collections import defaultdict 

class Graph: 
    def __init__(self): 
        self.graph = defaultdict(list)

    def addEdge(self,u,v):
        self.graph[u].append(v)

    def show(self) :
        for i in self.graph :
            print(line.index(i), ':', [line.index(j) for j in self.graph[i]])

    def replace(self, source, target) :
        for start in self.graph :
            for index in range(len(self.graph[start])) :
                if self.graph[start][index] == source :
                    self.graph[start][index] = target

    def BFS(self, source, target): 
        visited = {source}
        queue = [(source, 0)]
        while queue:
            s = queue.pop(0)
            if s[0] == target :
                return s[1]
            for i in self.graph[s[0]] :
                if i in visited : continue
                queue.append((i,s[1]+1))
                visited.add(i)

N, M = [int(i) for i in input().split()]

line = []
for i in reversed(range(N)) :
    line += zip([i for m in range(M)], range(M)) if not ((N-i+1)%2)\
       else zip([i for m in range(M)], reversed(range(M)))


g = Graph()

for index in range(len(line)) :
    for step in range(min(6, len(line)-index-1)) :
        g.addEdge(line[index], line[index+1+step])

# transes = {}
# while True :
#     i, j, target = [int(i) for i in input().split()]
#     if i == j == target == -1 :
#         break
#     transes[(i, j)] = line[target]

# for source in transes :
#     target = transes[source]
#     while target in transes :
#         target = transes[target]
#     g.replace(source, target)

while True :
    i, j, target = [int(i) for i in input().split()]
    if i == j == target == -1 :
        break
    g.replace((i, j), line[target])

# g.show()
# print("target :", line[-1])

print(g.BFS(line[0], line[-1]))