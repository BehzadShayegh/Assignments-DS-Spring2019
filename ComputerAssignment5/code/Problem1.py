from collections import defaultdict 

n = int(input())

class Graph: 
    def __init__(self): 
        self.graph = defaultdict(list)

    def addEdge(self,u,v):
        self.graph[u].append(v)
		
    def trainable(self, v, visited):
        if v in visited :
            return False
        visited.append(v)
        # print(visited)
        if len(visited) == n :
            return True
        for i in self.graph[v] :
            if self.trainable(i, visited) :
                return True
        visited.remove(v)
        return False

    def show(self) :
        for i in self.graph :
            print(i, ':', self.graph[i])

g = Graph()

words = []
for i in range(n) :
    newWord = input()
    while newWord in g.graph:
        newWord += '_' + newWord[-1]
    for word in words :
        if word[0] == newWord[-1] :
            g.addEdge(newWord, word)
        if word[-1] == newWord[0] :
            g.addEdge(word, newWord)
    words.append(newWord)

# print('--------------------')
# g.show()
# print('--------------------')

end = False

if not end :
    for word in words :
        if g.trainable(word, []) :
            print("possible")
            end = True
            break

if not end :
    print("not possible")