n = int(input())

buses = []
for i in range(n) :
    buses.append(input().split())

start, target = input().split()




from collections import defaultdict 

class Graph: 
    def __init__(self): 
        self.graph = defaultdict(list)

    def addEdge(self,u,v):
        self.graph[v].append(u)
        self.graph[u].append(v)

    def show(self) :
        for i in self.graph :
            print(i, ':', self.graph[i])

    def BFS(self, sorce, target): 
        if sorce == target :
            return 0
        visited = {sorce}
        queue = [(sorce, 0)]
        while queue:
            s = queue.pop(0)
            if s[0] == target :
                return s[1]
            for i in self.graph[s[0]] :
                if i in visited : continue
                queue.append((i,s[1]+1))
                visited.add(i)
        return -2

g = Graph()

for v in range(n) :
    for u in range(v+1, n) :
        for node in buses[v] :
            if node in buses[u] :
                g.addEdge(u, v)
                break

startLines = []
for index in range(n) :
    if start in buses[index] :
        startLines.append(index)

targetLines = []
for index in range(n) :
    if target in buses[index] :
        targetLines.append(index)

shortest = n
for s in startLines :
    for e in targetLines :
        shortest = min(g.BFS(s, e), shortest)

if shortest == n : shortest = -2
print(shortest+1)