needBooks = input().split('-')
maxBookNum = int(input())

def mood1(needBooks):
    currentBooks = []
    schoolDayNum = 0

    for book in needBooks :
        if book in currentBooks :
            continue
        
        currentBooks.append(book)
        schoolDayNum = schoolDayNum + 1

        if len(currentBooks) > maxBookNum :
            currentBooks.remove(currentBooks[0])
    
    return schoolDayNum

def mood2(needBooks):
    currentBooks = []
    schoolDayNum = 0

    index = -1
    for book in needBooks :
        index = index + 1
        if book in currentBooks :
            continue
        
        if len(currentBooks) > maxBookNum-1 :
            bookNextIndexs = dict()
            nextIndex = index - 1
            for nextBook in needBooks[index:] :
                nextIndex = nextIndex + 1
                if nextBook in currentBooks :
                    if nextBook not in bookNextIndexs :
                        bookNextIndexs[nextBook] = nextIndex
            
            forremove = currentBooks[0]
            
            try:
                forremove = max(bookNextIndexs.items(), key=lambda x:x[1])[0]
            except:
                pass
            
            for b in currentBooks :
                try:
                    bookNextIndexs[b]
                except:
                    forremove = b

            currentBooks.remove(forremove)

        currentBooks.append(book)
        schoolDayNum = schoolDayNum + 1
    return schoolDayNum

def mood3(needBooks):
    currentBooks = []
    schoolDayNum = 0

    for book in needBooks :
        if book in currentBooks :
            currentBooks.remove(book)
            currentBooks.append(book)
            continue

        currentBooks.append(book)
        schoolDayNum = schoolDayNum + 1

        if len(currentBooks) > maxBookNum :
            currentBooks.remove(currentBooks[0])

    return schoolDayNum

mood = int(input())
schoolDayNum = 0
if (mood == 1): schoolDayNum = mood1(needBooks)
if (mood == 2): schoolDayNum = mood2(needBooks)
if (mood == 3): schoolDayNum = mood3(needBooks)

print(schoolDayNum)