n,k = [ int(x) for x in input().split(' ') ]
parts = [ int(height) for height in input().split(' ') ]
moves = []
stops = []
totalStepsNum = 0

def turn(status, step) :
    return (status+step)%3

def turnNeed(status, step) :
    return 3-turn(status, step) if turn(status, step) != 0 else 0

def turnParts(index) :
    step = turnNeed(parts[index], moves[index])
    moves.append(moves[index]-stops[index+1]+step)
    stops.append(step)
    parts[index] = turn(parts[index], step+moves[index])
    return step

moves = [0]
stops = [ 0 for i in range(k)]
for index in range(n) :
    totalStepsNum += turnParts(index)

print(totalStepsNum)