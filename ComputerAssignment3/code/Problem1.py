n = int(input())
heights = [ int(height) for height in input().split() ]
leftTower = [ 0 for i in range(n) ]
rightTower = [ n-1 for i in range(n) ]

def mostLeftTower(index, ltIndex) :
    if (heights[index] >= heights[ltIndex] and ltIndex != 0) :
        return mostLeftTower(index, leftTower[ltIndex])
    else :
        return ltIndex

def mostRightTower(index, rtIndex) :
    if (heights[index] >= heights[rtIndex] and rtIndex != n-1) :
        return mostRightTower(index, rightTower[rtIndex])
    else :
        return rtIndex

for index in range(1,n) :
    if(heights[index] >= heights[index-1]) :
        leftTower[index] = mostLeftTower(index, index-1)
    else :
        leftTower[index] = index-1

for index in reversed(range(n-1)) :
    if(heights[index] >= heights[index+1]) :
        rightTower[index] = mostRightTower(index, index+1)
    else :
        rightTower[index] = index+1
    
for index in range(n) :
    leftDistance = index - leftTower[index]
    rightDistance = rightTower[index] - index
    maxDistances = max(leftDistance, rightDistance)
    print(maxDistances, end=' ')