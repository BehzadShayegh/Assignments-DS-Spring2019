n = int(input())
mineOwners = input().split()
minesNum = {'-1' : n+1}

for mineOwner in mineOwners :
    if mineOwner in minesNum :
        minesNum[mineOwner] += 1
    else :
        minesNum[mineOwner] = 1
mineOwners.append('-1')

targets = {str(n-1) : n}

def findTarget(index, targetIndex) :
    if minesNum[mineOwners[targetIndex]] > minesNum[mineOwners[index]] :
        return targetIndex
    else :
        return findTarget(index, targets[str(targetIndex)])

for index in reversed(range(n-1)) :
    targets[str(index)] = findTarget(index, index+1)

for index in range(n-1) :
    print(mineOwners[targets[str(index)]], end=' ')
print(mineOwners[targets[str(n-1)]])