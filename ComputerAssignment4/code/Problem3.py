import math

class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

    def set_left(self, left_node):
        self.left = left_node
    def set_right(self, right_node):
        self.right = right_node

    def in_order(self) :
        if self.left is None and self.right is None :
            return [self.data]
        
        if self.left is None :
            return [self.data] + self.right.in_order()

        if self.right is None :
            return self.left.in_order() + [self.data]

        return self.left.in_order() + [self.data] + self.right.in_order()

N = int(input())
nodes = [Node(int(data)) for data in input().split()]

for i in range(math.floor(N/2)) :
    nodes[i].set_left(nodes[2*(i+1)-1])
    if 2*(i+1) < N : nodes[i].set_right(nodes[2*(i+1)])

inOrder = zip(nodes[0].in_order(), [i for i in range(N)])
inOrder = sorted(inOrder, key=lambda x: x[0])

swaps = 0
index = 0
while index < N :
    prevIndex = inOrder[index][1]
    if index == prevIndex :
        index += 1
        continue
    else :
        inOrder[index] , inOrder[prevIndex] = inOrder[prevIndex] , inOrder[index]
    swaps += 1

print(swaps)