import heapq
import math

def _heappush_max(heap, item):
    heap.append(item)
    heapq._siftdown_max(heap, 0, len(heap)-1)

n, T = [int(i) for i in input().split()]
maxHeap = [int(i) for i in input().split()]
heapq._heapify_max(maxHeap)

result = 0
for i in range(T) :
    popPar = heapq._heappop_max(maxHeap)
    _heappush_max(maxHeap, math.floor(popPar/2))
    result += popPar
    
print(result)