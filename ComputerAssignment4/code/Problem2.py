class Node(object):
    def __init__(self, status = False, parent = None):
        self.status = status
        self.children = []
        if parent is not None:
            parent.add_child(self)

    def status(self) :
        return self.status

    def turn_on(self) :
        self.status = True

    def toggle(self) :
        self.status = not self.status
        for child in self.children :
            child.toggle()

    def report(self) :
        report = 1 if self.status else 0
        for child in self.children :
            report += child.report()
        return report

    def add_child(self, node):
        self.children.append(node)

N = int(input())
parents = [int(i)-1 for i in input().split()]
statuses = [(i == '1') for i in input().split()]

nodes = [Node()]
for i in range(N-1) :
    nodes.append(Node(parent = nodes[parents[i]]))

for i in range(N) :
    if statuses[i]:
        nodes[i].turn_on()

K = int(input())
for orderNumber in range(K) :
    order, target = input().split()
    target = int(target) - 1
    if order == 'report' :
        print(nodes[target].report())
    else :
        nodes[target].toggle()