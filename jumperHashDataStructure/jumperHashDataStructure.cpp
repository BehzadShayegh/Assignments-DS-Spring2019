#include <iostream>

using namespace std;

class HashDs {
    struct Element {
        bool exist;
        int data;
        int nextNeighborIndex;

        Element(int _data) : data(_data) {
            nextNeighborIndex = -1;
            exist = true;
        }
        Element() {
            exist = false;
            nextNeighborIndex = -1;
        }
        void print() {
            if (exist) cout << data << '(' << nextNeighborIndex << ")| ";
            else cout << " -  | ";
        }
        void kill() {
            exist = false;
            nextNeighborIndex = -1;
        }
    };

    private:
        int size;
        int numberOfElements;
        Element* list;
        int hashFunction(int _data) {
            return _data % size;
        }
        int newNeighbor(int index) {
            int i = (index+1) % size;
            while(i != index) {
                if(!list[i].exist) {
                    list[index].nextNeighborIndex = i;
                    return i;
                }
                i = (++i) % size;
            }
        }
        int findEmptyIndex(int hashValue) {
            if(!list[hashValue].exist)
                return hashValue;
            if(list[hashValue].nextNeighborIndex == -1)
                return newNeighbor(hashValue);
            return findEmptyIndex(list[hashValue].nextNeighborIndex);
        }

    public:
        HashDs(int _size) : size(_size) {
            list = new Element[size];
            numberOfElements = 0;
            for(int i = 0; i < size; i++)
                list[i] = Element();
        }
        bool isFill() {
            return numberOfElements == size;
        }
        bool insert(int _data) {
            if(isFill())
                return false;
            list[findEmptyIndex(hashFunction(_data))] = Element(_data);
            numberOfElements++;
            return true;
        }
        void print() {
            for(int i = 0; i < size; i++)
                list[i].print();
        }
        int search(int _data) {
            int index;
            int staticIndex = index = hashFunction(_data);
            do {
                if(index == -1)
                    return -1;
                if(list[index].exist && list[index].data == _data)
                    return index;
                index = list[index].nextNeighborIndex;
            } while(index != staticIndex);
            return -1;
        }
        void remove(int _data) {
            int index, prevIndex;
            int staticIndex = index = prevIndex = hashFunction(_data);
            do {
                if(list[index].exist && list[index].data == _data){
                    if(staticIndex == index && list[index].nextNeighborIndex != -1) {
                        int neighbor = list[index].nextNeighborIndex;
                        list[index] = list[neighbor];
                        list[neighbor].kill();
                    } else {
                        list[prevIndex].nextNeighborIndex = list[index].nextNeighborIndex;
                        list[index].kill();
                    }
                    return;
                }
                prevIndex = index;
                index = list[index].nextNeighborIndex;
            } while(index != -1);
        }
};

int main() {
    int size;
    cin >> size;
    HashDs hashDs(size);
    while(true) {
        int data;
        string command;
        cin >> command;
        if(command == "add") {
            cin >> data;
            if(!hashDs.insert(data))
                cout << "is Fill ..." << endl;
            hashDs.print();
            cout << endl;
        } else if(command == "search") {
            cin >> data;
            cout << hashDs.search(data) << endl;
        } else if(command == "remove") {
            cin >> data;
            hashDs.remove(data);
            hashDs.print();
            cout << endl;
        } else {
            cout << "ERROR!" << endl;
        }
    }
}